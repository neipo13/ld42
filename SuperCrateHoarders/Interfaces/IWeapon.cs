﻿using Nez;
using Nez.Textures;

namespace SuperCrateHoarders.Interfaces
{
    public interface IWeapon
    {
        void Fire(Direction dir);
        void setFlipX(bool flipped);
        string weaponName { get; }
        void RemoveSprites();
    }
}
