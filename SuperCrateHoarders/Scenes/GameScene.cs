﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.Textures;
using Nez.Tiled;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez.UI;
using SuperCrateHoarders.Entities;
using SuperCrateHoarders.Constants;
using SuperCrateHoarders.Util;
using SuperCrateHoarders.Components;
using System.IO;
using Microsoft.Xna.Framework.Audio;

namespace SuperCrateHoarders.Scenes
{
    public class GameScene : Scene
    {
        private List<Subtexture> tiles = new List<Subtexture>();
        public int lastCrateSpawnIndex = 0;
        public int score = 0;

        private UICanvas gameCanvas;
        private Container container;
        private Label scoreLabel;
        private Label scoreLabelShadow;
        private Label weaponName;

        private Table gameOverTable;
        private Label gameOver;
        private Label cratesHoarded;
        private Label highScore;
        private Label options;

        SimpleScreenShake shaker;

        private bool gameover = false;

        List<Subtexture> _subtextures;
        public List<Subtexture> subtextures
        {
            get
            {
                if (_subtextures == null)
                {
                    var texture = content.Load<Texture2D>("images/tiles");
                    _subtextures = Subtexture.subtexturesFromAtlas(texture, 16, 16);
                }
                return _subtextures;
            }
        }
        
        Dictionary<String, SoundEffect> soundEffects;
        public SoundEffect GetSoundEffect(string name)
        {
            if (soundEffects.ContainsKey(name))
            {
                return soundEffects[name];
            }
            return null;
        }

        public void PlaySoundEffect(string name)
        {
            if (soundEffects.ContainsKey(name))
            {
                soundEffects[name].Play();
            }
        }

        public void CameraShake(float shakeIntensity = 15f, float shakeDegredation = 0.9f, float shakeTime = 0.3f, Vector2 shakeDirection = default(Vector2))
        {
            shaker.shake(shakeIntensity, shakeDegredation, shakeTime, shakeDirection);
        }

        public override void initialize()
        {
            clearColor = new Color(57, 120, 168);
            addRenderer(new DefaultRenderer());

            Engine.gameEventEmitter.addObserver(GameEvents.GameOver, onGameOver);

            var tileTexture = content.Load<Texture2D>("images/tiles");
            tiles = Subtexture.subtexturesFromAtlas(tileTexture, 16, 16);

            var tiledMap = content.Load<TiledMap>($"tiled/test");
            var tiledEntity = this.createEntity("debugMap");
            var tileMapComponent = tiledEntity.addComponent(new TiledMapComponent(tiledMap, "collision"));
            tileMapComponent.setLayersToRender("collision");
            tileMapComponent.renderLayer = (int)Constants.RenderLayers.Background;
            tileMapComponent.physicsLayer = PhysicsLayers.tiles;
            var collisionLayer = tileMapComponent.collisionLayer;
            Components.Weapons.CrateGun.tileLayer = collisionLayer;

            var playerSpawn = tiledMap.getObjectGroup("playerSpawn").objects.First();
            Player.Create(this, Constants.TiledHelpers.AdjustTiledPosition(playerSpawn.position), collisionLayer);

            SpawnCrate(collisionLayer);


            var entity = addEntity(new Entity());
            gameCanvas = entity.addComponent(new UICanvas());
            gameCanvas.setRenderLayer(-10);
            var style = new LabelStyle(Color.White);
            var shadowStyle = new LabelStyle(Color.Gray);
            scoreLabel = new Label("0", style)
                .setAlignment(Align.center)
                .setFontScale(5f);
            scoreLabelShadow = new Label("0", shadowStyle)
                .setAlignment(Align.center)
                .setFontScale(5f);

            weaponName = new Label("", style)
                .setAlignment(Align.center)
                .setFontScale(2f);
            gameCanvas
                .stage
                .addElement(scoreLabelShadow)
                .setX(Engine.designWidth / 2 + 2)
                .setY(Engine.designHeight * 1 / 8 + 2);
            gameCanvas
                .stage
                .addElement(scoreLabel)
                .setX(Engine.designWidth / 2)
                .setY(Engine.designHeight * 1 / 8);
            gameCanvas
                .stage
                .addElement(weaponName);

            container = new Container();
            container.addElement(scoreLabelShadow);
            container.addElement(scoreLabel);
            gameCanvas.stage.addElement(container);

            var gameOverCanvas = entity.addComponent(new UICanvas());
            gameOverCanvas.setRenderLayer(-10);
            gameOver = new Label("GAME OVER", style)
                .setAlignment(Align.center)
                .setFontScale(4f);
            cratesHoarded = new Label($"Crates Hoarded: {score}", style)
                .setAlignment(Align.center)
                .setFontScale(4f);

            highScore = new Label($"High Score: {score}", style)
                .setAlignment(Align.center)
                .setFontScale(2f);
            options = new Label($"Press 'R' to restart!", style)
                .setAlignment(Align.center)
                .setFontScale(2f);

            gameOverTable = new Table();
            gameOverTable.setColor(new Color(Color.Gray, .5f));
            gameOverTable.setSize(Engine.designWidth, Engine.designHeight / 3);

            gameOverCanvas
                .stage
                .addElement(gameOverTable)
                .setX(0)
                .setY(Engine.designHeight / 3);

            gameOverTable.add(gameOver);
            gameOverTable.row();
            gameOverTable.add(cratesHoarded);
            gameOverTable.row();
            gameOverTable.add(highScore);
            gameOverTable.row();
            gameOverTable.add(options);

            gameOverTable.setIsVisible(false);


            var topBox = entity.addComponent(new BoxCollider(16 * 6, 16));
            topBox.physicsLayer = PhysicsLayers.offscreen_collider;
            topBox.localOffset = new Vector2(16 * 16, 0);
            var bottomBox = entity.addComponent(new BoxCollider(16 * 6, 16));
            bottomBox.physicsLayer = PhysicsLayers.offscreen_collider;
            bottomBox.localOffset = new Vector2(16 * 16, 20 * 16);
            bottomBox.isTrigger = true;
            bottomBox.name = Strings.OFFSCREEN_COLLIDER;

            var spawner = addSceneComponent<EnemySpawner>();
            spawner.SetCollisionLayer(collisionLayer);

            LoadSoundEffects();

        }

        public void LoadSoundEffects()
        {
            soundEffects?.Clear();
            soundEffects = new Dictionary<string, SoundEffect>();
            var sound = content.Load<SoundEffect>(Strings.ENEMY_DEAD_SOUND);
            soundEffects.Add(Strings.ENEMY_DEAD_SOUND, sound);

            sound = content.Load<SoundEffect>(Strings.ENEMY_HIT_SOUND);
            soundEffects.Add(Strings.ENEMY_HIT_SOUND, sound);

            sound = content.Load<SoundEffect>(Strings.ENEMY_POWERUP_SOUND);
            soundEffects.Add(Strings.ENEMY_POWERUP_SOUND, sound);

            sound = content.Load<SoundEffect>(Strings.ENEMY_SPAWN_SOUND);
            soundEffects.Add(Strings.ENEMY_SPAWN_SOUND, sound);

            sound = content.Load<SoundEffect>(Strings.JUMP_SOUND);
            soundEffects.Add(Strings.JUMP_SOUND, sound);

            sound = content.Load<SoundEffect>(Strings.PISTOL_SOUND);
            soundEffects.Add(Strings.PISTOL_SOUND, sound);

            sound = content.Load<SoundEffect>(Strings.PLAYER_DEAD_SOUND);
            soundEffects.Add(Strings.PLAYER_DEAD_SOUND, sound);

            sound = content.Load<SoundEffect>(Strings.SHOTGUN_SOUND);
            soundEffects.Add(Strings.SHOTGUN_SOUND, sound);

            sound = content.Load<SoundEffect>(Strings.CRATE_PICKUP_SOUND);
            soundEffects.Add(Strings.CRATE_PICKUP_SOUND, sound);

            sound = content.Load<SoundEffect>(Strings.EXPLOSION_SOUND);
            soundEffects.Add(Strings.EXPLOSION_SOUND, sound);

            sound = content.Load<SoundEffect>(Strings.GRENADE_LAUNCHER_SOUND);
            soundEffects.Add(Strings.GRENADE_LAUNCHER_SOUND, sound);

            sound = content.Load<SoundEffect>(Strings.BAZOOKA_SOUND);
            soundEffects.Add(Strings.BAZOOKA_SOUND, sound);

            sound = content.Load<SoundEffect>(Strings.LAZER_SOUND);
            soundEffects.Add(Strings.LAZER_SOUND, sound);
        }


        public Entity SpawnCrate(TiledTileLayer layer)
        {
            int id = 1;
            int x = 0;
            int y = 0;
            bool aboveClear = false;
            bool belowSolid = false;
            bool aboveRightClear = false;
            bool aboveLeftClear = false;
            while (id == 1 || !aboveClear || !belowSolid || !aboveLeftClear || !aboveRightClear)
            {
                x = Nez.Random.range(1, 31);
                y = Nez.Random.range(2, 17);
                id = layer.getTile(x, y)?.id ?? 0;
                if (id != 0) continue;

                aboveClear = (layer.getTile(x, y - 1)?.id ?? 0) == 0;
                aboveLeftClear = (layer.getTile(x - 1, y - 1)?.id ?? 0) == 0;
                aboveRightClear = (layer.getTile(x + 1, y - 1)?.id ?? 0) == 0;
                belowSolid = (layer.getTile(x, y + 1)?.id ?? 0) != 0;

            }
            return Crate.Create(this, new Vector2(x * 16, y * 16) + Constants.TiledHelpers.tiledPositionAdjustment2);
        }

        public void DisplayWeaponName(Vector2 position, string display)
        {
            weaponName.setText(display);
            weaponName.setX(position.X).setY(position.Y);
            weaponName.tween("y", position.Y - 16, 0.5f).setCompletionHandler((f) => weaponName.setText("")).start();
        }

        public override void onStart()
        {
            base.onStart();
            shaker = camera.addComponent(new SimpleScreenShake(camera.position));
        }

        public void onGameOver()
        {
            container.setIsVisible(false);
            gameOverTable.setIsVisible(true);
            var high = 0;
            if (File.Exists(Strings.HIGHSCORE_FILE_NAME))
            {
                using (BinaryReader reader = new BinaryReader(File.Open(Strings.HIGHSCORE_FILE_NAME, FileMode.Open)))
                {
                    high = reader.ReadInt32();
                }
            }
            else
            {
                WriteHighScore(score);
                high = score;
            }
            if(score > high)
            {
                high = score;
                WriteHighScore(score);
            }
            highScore.setText($"High Score: {high}");
            cratesHoarded.setText($"Crates Hoarded: {score}");
            gameover = true;
        }

        public void WriteHighScore(int high)
        {
            using (BinaryWriter writer = new BinaryWriter(File.Open(Strings.HIGHSCORE_FILE_NAME, FileMode.Create)))
            {
                writer.Write(score);
            }
        }

        public override void update()
        {
            base.update();
            scoreLabel.setText(score.ToString());
            scoreLabelShadow.setText(score.ToString());

            if(gameover && Input.isKeyPressed(Microsoft.Xna.Framework.Input.Keys.R))
            {
                Core.scene = new GameScene();
            }
        }

        public override void unload()
        {
            base.unload();
        }
    }
}
