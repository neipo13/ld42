﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Microsoft.Xna.Framework;
using SuperCrateHoarders.Scenes;
using Nez.Sprites;
using SuperCrateHoarders.Constants;
using SuperCrateHoarders.Components;

namespace SuperCrateHoarders.Entities
{
    public class Explosive : Entity
    {
        public Explosive() : base() { }
        public Explosive(string s) : base(s) { }
        public override void onRemovedFromScene()
        {
            var s = (GameScene)scene;
            s.CameraShake(35f);
            base.onRemovedFromScene();
            //spawn explosion
            CreateExplosion(s, position);

        }

        public static Entity CreateExplosion(GameScene scene, Vector2 position, float lifespan = 0.3f, float scalar = 6f)
        {
            var e = scene.addEntity(new Entity(Strings.EXPLOSIVE));
            e.position = position;
            

            var s = e.addComponent(new Sprite(scene.subtextures[97]));
            s.renderLayer = (int)RenderLayers.Explosions;

            var col = e.addComponent(new BoxCollider(16, 16));
            col.isTrigger = true;
            col.name = Strings.BULLET;
            col.physicsLayer = PhysicsLayers.bullet;
            col.collidesWithLayers = PhysicsLayers.enemy_hurt;

            e.tween("scale", new Vector2(scalar), lifespan).setEaseType(Nez.Tweens.EaseType.ExpoOut).start();

            e.addComponent(new DestroyOnTimer(lifespan));

            scene.PlaySoundEffect(Strings.EXPLOSION_SOUND);

            return e;
        }
    }
}
