﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using Nez.Tiled;
using SuperCrateHoarders.Components;
using SuperCrateHoarders.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SuperCrateHoarders.Components.PlayerAnimationController;

namespace SuperCrateHoarders.Entities
{
    public class BasicEnemy
    {
        public static Entity Create(Scene scene, Vector2 position, TiledTileLayer collisionLayer)
        {
            var e = scene.addEntity(new Entity("enemy"));
            e.position = position;
            
            var sprite = e.addComponent(new Sprite<PlayerAnimations>());
            sprite.renderLayer = (int)Constants.RenderLayers.Default;
            sprite.followParentEntityRotation = false;
            var playerAnimationController = e.addComponent(new PlayerAnimationController(sprite, 48));

            var box = e.addComponent(new BoxCollider(16, 16));
            box.name = "movebox";
            box.physicsLayer = PhysicsLayers.enemy_move;
            box.collidesWithLayers = PhysicsLayers.tiles;
            e.addComponent(new TiledMapMover(collisionLayer));

            var hitbox = e.addComponent(new BoxCollider(16, 16));
            hitbox.name = Strings.HITBOX;
            hitbox.physicsLayer = PhysicsLayers.enemy_hit;
            hitbox.collidesWithLayers = PhysicsLayers.player_hurt | PhysicsLayers.offscreen_collider;
            hitbox.active = true;
            hitbox.isTrigger = true;

            var hurtBox = e.addComponent(new BoxCollider(16, 16));
            hurtBox.name = Strings.HURTBOX;
            hurtBox.physicsLayer = PhysicsLayers.enemy_hurt;
            hurtBox.collidesWithLayers = PhysicsLayers.bullet;

            var controller = e.addComponent<BasicEnemyController>();
            controller.direction = Nez.Random.minusOneToOne() > 0f ? 1 : -1;
            e.addComponent<BulletCheck>();

            return e;
        }

        public static Entity CreateDeadEnemy(Scene scene, Vector2 position, int startFrame, Vector2 direction)
        {
            var e = scene.addEntity(new Entity());
            e.position = position;

            var sprite = e.addComponent(new Sprite<PlayerAnimations>());
            sprite.renderLayer = (int)Constants.RenderLayers.Default;
            sprite.followParentEntityRotation = false;
            var playerAnimationController = e.addComponent(new PlayerAnimationController(sprite, startFrame));

            var c = e.addComponent<DeadPlayerController>();
            c.rotation = (float)Math.PI / 16;
            c.moveSpeed *= 0.5f;
            c.velocity = direction * (c.moveSpeed/10f);

            return e;
        }
    }
}
