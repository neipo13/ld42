﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using Nez.Tiled;
using SuperCrateHoarders.Components;
using SuperCrateHoarders.Components.Weapons;
using SuperCrateHoarders.Constants;
using SuperCrateHoarders.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static SuperCrateHoarders.Components.PlayerAnimationController;

namespace SuperCrateHoarders.Entities
{
    public class Player
    {
        public static Entity Create(Scene scene, Vector2 position, TiledTileLayer collisionLayer, int playerId=0)
        {
            var e = scene.addEntity(new Entity(Strings.PLAYER));
            e.position = position;

            e.addComponent(new InputHandler(playerId));
            var sprite = e.addComponent(new Sprite<PlayerAnimations>());
            sprite.renderLayer = (int)Constants.RenderLayers.Default;
            sprite.followParentEntityRotation = false;
            var playerAnimationController = e.addComponent(new PlayerAnimationController(sprite));

            var box = e.addComponent(new BoxCollider(16, 16));
            box.name = Strings.MOVEBOX;
            box.physicsLayer = PhysicsLayers.move;
            box.collidesWithLayers = PhysicsLayers.tiles | PhysicsLayers.offscreen_collider;
            e.addComponent(new TiledMapMover(collisionLayer));

            var hitbox = e.addComponent(new BoxCollider(18, 18));
            hitbox.name = Strings.HITBOX;
            hitbox.physicsLayer = PhysicsLayers.player_hit;
            hitbox.collidesWithLayers = PhysicsLayers.crate;
            hitbox.active = true;
            hitbox.isTrigger = true;

            var hurtBox = e.addComponent(new BoxCollider(12, 12));
            hurtBox.name = Strings.HURTBOX;
            hurtBox.physicsLayer = PhysicsLayers.player_hurt;
            hurtBox.collidesWithLayers = PhysicsLayers.enemy_move;

            var controller = e.addComponent<PlayerController>();
            controller.ChangeWeapon(e.addComponent<Pistol>());
            e.addComponent(new CrateCheck(collisionLayer));


            return e;
        }

        public static Entity CreateDeadPlayer(Scene scene, Vector2 position)
        {
            var e = scene.addEntity(new Entity(Strings.PLAYER));
            e.position = position;

            var sprite = e.addComponent(new Sprite<PlayerAnimations>());
            sprite.renderLayer = (int)Constants.RenderLayers.Default;
            sprite.followParentEntityRotation = false;
            var playerAnimationController = e.addComponent(new PlayerAnimationController(sprite));

            e.addComponent<DeadPlayerController>();

            return e;
        }
    }
}
