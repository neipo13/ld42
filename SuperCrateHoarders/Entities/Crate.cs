﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using Nez.Tiled;
using SuperCrateHoarders.Constants;
using SuperCrateHoarders.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperCrateHoarders.Entities
{
    public class Crate
    {
        private static Subtexture _subtexture;
        private static Subtexture _usedSubtexture;
        private static Subtexture GetSubtexture(Scene scene)
        {
            var gameScene = (GameScene)scene;
            return gameScene.subtextures[17];
        }
        private static Subtexture GetUsedSubtexture(Scene scene)
        {
            var gameScene = (GameScene)scene;
            return gameScene.subtextures[18];
        }

        public static void UnloadSubtextures()
        {
            _subtexture = null;
            _usedSubtexture = null;
        }

        public static Entity Create(Scene scene, Vector2 position)
        {
            var e = scene.addEntity(new Entity("crate"));
            e.position = position;

            var sprite = e.addComponent(new Sprite(GetSubtexture(scene)));
            sprite.renderLayer = (int)Constants.RenderLayers.Background;
            sprite.name = Strings.NEW_CRATE;

            var usedSprite = e.addComponent(new Sprite(GetUsedSubtexture(scene)));
            usedSprite.enabled = false;
            usedSprite.renderLayer = (int)Constants.RenderLayers.Background;
            usedSprite.name = Strings.USED_CRATE;


            var box = e.addComponent(new BoxCollider(16, 16));
            box.isTrigger = true;
            box.name = Constants.Strings.CRATE;
            box.physicsLayer = PhysicsLayers.crate;
            box.collidesWithLayers = PhysicsLayers.player_hit;
            box.active = true;

            return e;
        }
    }
}
