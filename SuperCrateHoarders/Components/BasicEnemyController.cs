﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez.Tiled;
using Nez.Audio;

namespace SuperCrateHoarders.Components
{
    public class BasicEnemyController : Component, IUpdatable
    {
        TiledMapMover mover;
        TiledMapMover.CollisionState collisionState = new TiledMapMover.CollisionState();
        BoxCollider moveBox;
        Sprite<PlayerAnimationController.PlayerAnimations> sprite;
        public PlayerAnimationController animationController;
        BoxCollider hitBox;
        
        ColliderTriggerHelper triggerHelper;

        AudioSource swingAudioSource;


        public float moveSpeed = 75f;
        Vector2 velocity;
        float gravity = 800f;
        float runAccel = 1000f;
        float friction = 400f;
        Vector2 maxSpeedVec;
        public int direction = 1;
        Vector2 input;
        bool jump = false;
        bool isGrounded => collisionState.below;
        bool isMovingHorizontal => velocity.X > 0f || velocity.X < 0f;
        float jumpHeight = 16 * 2 + 4; //16px tiles * tiles high + buffer
        bool canJumpThisFrame
        {
            get { return collisionState.below; }
        }

        public void ChangeAnimationContoller(PlayerAnimationController controller)
        {
            animationController = controller;
            sprite = controller.sprite;
        }

        public override void onAddedToEntity()
        {
            mover = entity.getComponent<TiledMapMover>();
            animationController = entity.getComponent<PlayerAnimationController>();
            sprite = animationController.sprite;
            var boxes = entity.getComponents<BoxCollider>();
            moveBox = boxes.FirstOrDefault(c => c.name == Constants.Strings.MOVEBOX);
            hitBox = boxes.FirstOrDefault(c => c.name == Constants.Strings.HITBOX);
            velocity = Vector2.Zero;
            maxSpeedVec = new Vector2(moveSpeed * 2f, moveSpeed * 3f);
            triggerHelper = new ColliderTriggerHelper(entity);
            //Engine.gameEventEmitter.addObserver(GameEvents.GameOver, OnGameOver);

            //swingAudioSource = new AudioSource();
            //var effect = entity.scene.content.Load<SoundEffect>("audio/slash_sound");
            //swingAudioSource.addSoundEffect(effect);
            //swingAudioSource.setPitchRange(0.8f, 1.2f);

        }

        public void OnGameOver()
        {
            this.enabled = false;
        }
        public void update()
        {
            if (Time.timeScale < 0.1f) return;
            updateInput();
            move();
            triggerHelper.update();
            if (velocity.X > 0)
            {
                sprite.flipX = false;
            }
            else if (velocity.X < 0) sprite.flipX = true;

            if (!isGrounded || !isMovingHorizontal)
            {
                animationController.Play(PlayerAnimationController.PlayerAnimations.Idle);
            }
            else if (isGrounded && isMovingHorizontal)
            {
                animationController.Play(PlayerAnimationController.PlayerAnimations.Run);
            }        
        }

        public virtual void updateInput()
        {
            if((collisionState.left && direction < 0) ||
                (collisionState.right && direction > 0))
            {
                direction *= -1;
            }

            if(Nez.Random.range(0, 100) == 1)
            {
                jump = true;
            }
            else
            {
                jump = false;
            }
        }

        public void move()
        {
            if (Math.Abs(velocity.X) > moveSpeed && Math.Sign(velocity.X) == direction)
                velocity.X = Mathf.approach(velocity.X, moveSpeed * direction, friction * Time.deltaTime);  //Reduce back from beyond the max speed
            else
                velocity.X = Mathf.approach(velocity.X, moveSpeed * direction, runAccel * Time.deltaTime);
            //jump
            if (jump)
            {
                if (canJumpThisFrame) Jump();
            }


            //apply gravity
            velocity.Y += gravity * Time.deltaTime;

            velocity = Vector2.Clamp(velocity, -maxSpeedVec, maxSpeedVec);

            var oldPos = entity.position;
            mover.move(velocity * Time.deltaTime, moveBox, collisionState);

            if (collisionState.below)
            {
                sprite.rotation = 0;
            }
            else
            {
                sprite.rotation += (float)Math.PI / 8 * (sprite.flipX ? -1 : 1);
            }

            // dont let gravity just build while you're grounded
            if (collisionState.above || collisionState.below)
                velocity.Y = 0;
        }

        void Jump()
        {
            velocity.Y = -Mathf.sqrt(2 * jumpHeight * gravity);
            var scene = (Scenes.GameScene)entity.scene;
            scene.PlaySoundEffect(Constants.Strings.JUMP_SOUND);
        }
    }
}
