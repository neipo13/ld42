﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Microsoft.Xna.Framework;
using Nez.Sprites;

namespace SuperCrateHoarders.Components
{
    public class DeadPlayerController : Component, IUpdatable
    {
        Mover mover;
        Sprite sprite;

        public float moveSpeed = 400f;
        public Vector2 velocity;
        float gravity = 800f;
        public float rotation = (float)Math.PI / 4;

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            mover = entity.addComponent<Mover>();
            velocity.Y += -moveSpeed;
            sprite = entity.getComponent<Sprite>();
        }
        public void update()
        {
            if(entity.position.Y > 600)
            {
                entity.destroy();
                return;
            }
            sprite.rotation +=  rotation * (sprite.flipX ? -1 : 1);

            velocity.Y += gravity * Time.deltaTime;

            mover.move(velocity * Time.deltaTime, out CollisionResult result);
        }
    }
}
