﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using SuperCrateHoarders.Constants;
using SuperCrateHoarders.Interfaces;
using SuperCrateHoarders.Scenes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperCrateHoarders.Components.Weapons
{
    public class DualPistols : Component, IWeapon, IUpdatable
    {
        public string weaponName => Strings.DUAL_PISTOLS;

        private float bulletSpeed = 500f;

        private Sprite sprite;
        private Sprite sprite2;

        public float cooldown = 0.15f;
        public float cooldown_timer = -1f;

        public void setFlipX(bool flipped)
        {
        }
        Subtexture subtexture
        {
            get
            {
                var scene = (Scenes.GameScene)this.entity.scene;
                return scene.subtextures[80];
            }
        }


        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            sprite = entity.addComponent(new Sprite(subtexture));
            sprite.localOffset = new Vector2(8, 0);
            sprite.renderLayer = (int)RenderLayers.Weapons;
            sprite2 = entity.addComponent(new Sprite(subtexture));
            sprite2.localOffset = new Vector2(-8, 0);
            sprite2.renderLayer = (int)RenderLayers.Weapons;
            sprite2.flipX = true;
        }


        public void Fire(Direction dir)
        {
            if (cooldown_timer > 0f) return;
            AddBullet(1);
            AddBullet(-1);

            var scene = (GameScene)entity.scene;
            scene.PlaySoundEffect(Strings.PISTOL_SOUND);
            Core.schedule(0.02f, (timer) => scene.PlaySoundEffect(Strings.PISTOL_SOUND));
            scene.CameraShake(3f);

            cooldown_timer = cooldown;
        }

        private void AddBullet(int dir)
        {
            //spawn a new bullet in the scene
            var bullet = entity.scene.addEntity(new Entity(Strings.BULLET));
            bullet.position = entity.position;

            float x = bulletSpeed * dir;
            var mover = bullet.addComponent(new ConsistentVelocityMover(new Vector2(x, 0), (r) => bullet.destroy()));

            var wallCollider = bullet.addComponent(new BoxCollider(8, 4));
            wallCollider.physicsLayer = PhysicsLayers.move;
            wallCollider.collidesWithLayers = PhysicsLayers.tiles;


            var enCollider = bullet.addComponent(new BoxCollider(8, 4));
            enCollider.isTrigger = true;
            enCollider.name = Strings.BULLET;
            enCollider.physicsLayer = PhysicsLayers.bullet;
            enCollider.collidesWithLayers = PhysicsLayers.enemy_hurt;

            var bs = bullet.addComponent(new PrototypeSprite(8, 4));
            bs.color = new Color(244, 180, 27);
        }

        public void update()
        {
            if (cooldown_timer > 0f) cooldown_timer -= Time.deltaTime;
        }
        

        public override void onRemovedFromEntity()
        {
            base.onRemovedFromEntity();

        }

        public void RemoveSprites()
        {
            entity.removeComponent(sprite);
            entity.removeComponent(sprite2);
        }
    }
}
