﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using SuperCrateHoarders.Constants;
using SuperCrateHoarders.Entities;
using SuperCrateHoarders.Interfaces;

namespace SuperCrateHoarders.Components.Weapons
{
    public class RocketLauncher : Component, IWeapon, IUpdatable
    {
        public string weaponName => Strings.ROCKET_LAUNCHER;


        private float startSpeed = 200f;
        private float endSpeed = 2000f;

        private Sprite sprite;

        public float cooldown = 0.9f;
        public float cooldown_timer = -1f;


        public void setFlipX(bool flipped)
        {
            if (sprite == null) return;
            sprite.flipX = flipped;
            if (flipped)
            {
                sprite.localOffset = new Vector2(-8, 0);
            }
            else
            {
                sprite.localOffset = new Vector2(8, 0);
            }

        }
        Subtexture subtexture
        {
            get
            {
                var scene = (Scenes.GameScene)this.entity.scene;
                return scene.subtextures[85];
            }
        }

        Subtexture bulletTexture
        {
            get
            {
                var scene = (Scenes.GameScene)this.entity.scene;
                return scene.subtextures[96];

            }
        }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            sprite = entity.addComponent(new Sprite(subtexture));
            sprite.localOffset = new Vector2(8, 0);
            sprite.renderLayer = -5;
        }


        public void Fire(Direction dir)
        {
            if (cooldown_timer > 0f) return; if (cooldown_timer > 0f) return;
            //spawn a new bullet in the scene
            AddBullet(dir);

            var scene = (Scenes.GameScene)entity.scene;
            scene.PlaySoundEffect(Strings.BAZOOKA_SOUND);
            scene.CameraShake(25f);

            cooldown_timer = cooldown;
        }

        private void AddBullet(Direction dir)
        {
            //spawn a new bullet in the scene
            var bullet = entity.scene.addEntity(new Explosive("bullet"));
            bullet.position = entity.position;

            float x = startSpeed * (dir == Direction.Left ? -1 : 1);
            float xEnd = endSpeed * (dir == Direction.Left ? -1 : 1);
            var mover = bullet.addComponent(new ConsistentVelocityMover(new Vector2(x, 0), (r) =>
            {
                bullet.destroy();
                //spawn explosion!
            }));

            mover.tween("velocity", new Vector2(xEnd, 0), 1f).start();

            var wallCollider = bullet.addComponent(new BoxCollider(12, 2));
            wallCollider.physicsLayer = PhysicsLayers.move;
            wallCollider.collidesWithLayers = PhysicsLayers.tiles;


            var enCollider = bullet.addComponent(new BoxCollider(12, 2));
            enCollider.isTrigger = true;
            enCollider.name = Strings.BULLET;
            enCollider.physicsLayer = PhysicsLayers.bullet;
            enCollider.collidesWithLayers = PhysicsLayers.enemy_hurt;

            var bs = bullet.addComponent(new Sprite(bulletTexture));
            bs.flipX = sprite.flipX;
            
        }

        public void RemoveSprites()
        {
            entity.removeComponent(sprite);
        }

        public void update()
        {
            if (cooldown_timer > 0f) cooldown_timer -= Time.deltaTime;
        }
    }
}
