﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using SuperCrateHoarders.Constants;
using SuperCrateHoarders.Interfaces;

namespace SuperCrateHoarders.Components.Weapons
{
    public class Shotgun : Component, IWeapon, IUpdatable
    {
        public string weaponName => Strings.SHOTGUN;


        private float maxBulletSpeed = 800f;
        private float minBulletSpeed = 600f;
        private float friction = 2000f;

        private float numOfShots = 5;

        private Sprite sprite;

        public float cooldown = 0.6f;
        public float cooldown_timer = -1f;

        public float maxAngle = 0.3f;

        public void setFlipX(bool flipped)
        {
            if (sprite == null) return;
            sprite.flipX = flipped;
            if (flipped)
            {
                sprite.localOffset = new Vector2(-8, 0);
            }
            else
            {
                sprite.localOffset = new Vector2(8, 0);
            }

        }
        Subtexture subtexture
        {
            get
            {
                var scene = (Scenes.GameScene)this.entity.scene;
                return scene.subtextures[82];
            }
        }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            sprite = entity.addComponent(new Sprite(subtexture));
            sprite.localOffset = new Vector2(8, 0);
            sprite.renderLayer = -5;
        }


        public void Fire(Direction dir)
        {
            if (cooldown_timer > 0f) return;
            int x = dir == Direction.Left ? -1 : 1;
            for (int i = 0;  i < numOfShots; i++)
            {
                var angle = Nez.Random.range(-maxAngle, maxAngle);
                var direction = new Vector2(x, angle);
                direction.Normalize();
                AddBullet(direction);
            }
            var scene = (Scenes.GameScene)entity.scene;
            scene.PlaySoundEffect(Strings.SHOTGUN_SOUND);
            cooldown_timer = cooldown;
            scene.CameraShake(20f);
        }

        private void AddBullet(Vector2 dir)
        {
            //spawn a new bullet in the scene
            var bullet = entity.scene.addEntity(new Entity(Strings.BULLET));
            bullet.position = entity.position;

            var speed = Nez.Random.range(minBulletSpeed, maxBulletSpeed);

            var mover = bullet.addComponent(new ConsistentVelocityMover(dir * speed, (r) => bullet.destroy(), friction));

            var wallCollider = bullet.addComponent(new BoxCollider(8, 4));
            wallCollider.physicsLayer = PhysicsLayers.move;
            wallCollider.collidesWithLayers = PhysicsLayers.tiles;


            var enCollider = bullet.addComponent(new BoxCollider(8, 4));
            enCollider.isTrigger = true;
            enCollider.name = Strings.BULLET;
            enCollider.physicsLayer = PhysicsLayers.bullet;
            enCollider.collidesWithLayers = PhysicsLayers.enemy_hurt;

            var bs = bullet.addComponent(new PrototypeSprite(8, 4));
            bs.color = new Color(244, 180, 27);
        }

        public void RemoveSprites()
        {
            entity.removeComponent(sprite);
        }

        public void update()
        {
            if (cooldown_timer > 0f) cooldown_timer -= Time.deltaTime;
        }
    }
}
