﻿using Nez;
using SuperCrateHoarders.Interfaces;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez.Textures;
using Nez.Sprites;
using SuperCrateHoarders.Constants;
using Microsoft.Xna.Framework.Graphics;
using SuperCrateHoarders.Scenes;

namespace SuperCrateHoarders.Components.Weapons
{
    public class Pistol : Component, IWeapon, IUpdatable
    {
        public string weaponName => Strings.PISTOL;

        private float bulletSpeed = 500f;

        private Sprite sprite;

        public float cooldown = 0.1f;
        public float cooldown_timer = -1f;

        public void setFlipX(bool flipped)
        {
            if (sprite == null) return;
            sprite.flipX = flipped;
            if (flipped)
            {
                sprite.localOffset = new Vector2(-8, 0);
            }
            else
            {
                sprite.localOffset = new Vector2(8, 0);
            }

        }
        Subtexture subtexture
        {
            get
            {
                var scene = (Scenes.GameScene)this.entity.scene;
                return scene.subtextures[80];
            }
        }


        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            sprite = entity.addComponent(new Sprite(subtexture));
            sprite.localOffset = new Vector2(8, 0);
            sprite.renderLayer = -5;
        }


        public void Fire(Direction dir)
        {
            if (cooldown_timer > 0f) return;
            //spawn a new bullet in the scene
            var bullet = entity.scene.addEntity(new Entity("bullet"));
            bullet.position = entity.position;

            float x = bulletSpeed * (dir == Direction.Left ? -1 : 1);
            var mover = bullet.addComponent(new ConsistentVelocityMover(new Vector2(x, 0), (r) => bullet.destroy()));

            var wallCollider = bullet.addComponent(new BoxCollider(8, 4));
            wallCollider.physicsLayer = PhysicsLayers.move;
            wallCollider.collidesWithLayers = PhysicsLayers.tiles;


            var enCollider = bullet.addComponent(new BoxCollider(8, 4));
            enCollider.isTrigger = true;
            enCollider.name = Strings.BULLET;
            enCollider.physicsLayer = PhysicsLayers.bullet;
            enCollider.collidesWithLayers = PhysicsLayers.enemy_hurt;

            var bs = bullet.addComponent(new PrototypeSprite(8, 4));
            bs.color = new Color(244, 180, 27);

            var scene = (GameScene)entity.scene;
            scene.PlaySoundEffect(Strings.PISTOL_SOUND);
            scene.CameraShake(2f);

            cooldown_timer = cooldown;
        }

        public void update()
        {
            if (cooldown_timer > 0f) cooldown_timer -= Time.deltaTime;
        }

        public override void onRemovedFromEntity()
        {
            base.onRemovedFromEntity();
        }
        public void RemoveSprites()
        {
            entity.removeComponent(sprite);
        }
    }
}
