﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using SuperCrateHoarders.Constants;
using SuperCrateHoarders.Interfaces;

namespace SuperCrateHoarders.Components.Weapons
{
    public class AlienBlaster : Component, IWeapon, IUpdatable
    {
        public string weaponName => Strings.ALIEN_BLASTER;


        private float bulletSpeed = 1200f;

        private Sprite sprite;

        public float cooldown = 0.3f;
        public float cooldown_timer = -1f;


        public void setFlipX(bool flipped)
        {
            if (sprite == null) return;
            sprite.flipX = flipped;
            if (flipped)
            {
                sprite.localOffset = new Vector2(-8, 0);
            }
            else
            {
                sprite.localOffset = new Vector2(8, 0);
            }

        }
        Subtexture subtexture
        {
            get
            {
                var scene = (Scenes.GameScene)this.entity.scene;
                return scene.subtextures[84];
            }
        }

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            sprite = entity.addComponent(new Sprite(subtexture));
            sprite.localOffset = new Vector2(8, 0);
            sprite.renderLayer = -5;
        }


        public void Fire(Direction dir)
        {
            if (cooldown_timer > 0f) return; if (cooldown_timer > 0f) return;
            //spawn a new bullet in the scene
            AddBullet(dir);

            var scene = (Scenes.GameScene)entity.scene;
            scene.PlaySoundEffect(Strings.LAZER_SOUND);
            scene.CameraShake(2f);

            cooldown_timer = cooldown;
        }

        private void AddBullet(Direction dir)
        {
            //spawn a new bullet in the scene
            var bullet = entity.scene.addEntity(new Entity("bullet"));
            bullet.position = entity.position;

            var hp = 2;

            float x = bulletSpeed * (dir == Direction.Left ? -1 : 1);
            var velocity = new Vector2();
            ConsistentVelocityMover mover = null;
            mover = bullet.addComponent(new ConsistentVelocityMover(new Vector2(x, 0), (r) =>
            {
                hp--;
                if (hp <= 0)
                {
                    bullet.destroy();
                }
                mover.velocity *= -1;
            }));

            var wallCollider = bullet.addComponent(new BoxCollider(12, 2));
            wallCollider.physicsLayer = PhysicsLayers.move;
            wallCollider.collidesWithLayers = PhysicsLayers.tiles;


            var enCollider = bullet.addComponent(new BoxCollider(12, 2));
            enCollider.isTrigger = true;
            enCollider.name = Strings.BULLET;
            enCollider.physicsLayer = PhysicsLayers.bullet;
            enCollider.collidesWithLayers = PhysicsLayers.enemy_hurt;

            var bs = bullet.addComponent(new PrototypeSprite(12, 2));
            bs.color = new Color(182, 213, 60);
        }

        public void RemoveSprites()
        {
            entity.removeComponent(sprite);
        }

        public void update()
        {
            if (cooldown_timer > 0f) cooldown_timer -= Time.deltaTime;
        }
    }
}
