﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Nez.Sprites;
using Nez.Textures;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SuperCrateHoarders.Scenes;

namespace SuperCrateHoarders.Components
{
    public class PlayerAnimationController : Component
    {
        public enum PlayerAnimations
        {
            Idle,
            Run
        }
        public Sprite<PlayerAnimations> sprite;
        static List<Subtexture> _subtextures;
        public int startFrame;
        List<Subtexture> subtextures
        {
            get
            {
                var scene = (GameScene)this.entity.scene;
                return scene.subtextures;
            }
        }
        public PlayerAnimationController(Sprite<PlayerAnimations> sprite, int startFrame=32)
        {
            this.sprite = sprite;
            this.startFrame = startFrame;
        }


        public override void onAddedToEntity()
        {
            base.onAddedToEntity();

            var runSpriteAnim = new SpriteAnimation(subtextures.GetRange(startFrame + 4, 5));
            runSpriteAnim.fps = 16;
            var runAnim = sprite.addAnimation(PlayerAnimations.Run, runSpriteAnim);
            var idleAnim = sprite.addAnimation(PlayerAnimations.Idle, new SpriteAnimation(subtextures.GetRange(startFrame, 4)));

            sprite.play(PlayerAnimations.Idle);
        }

        public override void onRemovedFromEntity()
        {
            base.onRemovedFromEntity();
            sprite = null;
            _subtextures = null;
        }

        public void Play(PlayerAnimations animation)
        {
            var currentAnimation = sprite.currentAnimation;
            if (currentAnimation != animation)
                sprite.play(animation);
        }
    }
}
