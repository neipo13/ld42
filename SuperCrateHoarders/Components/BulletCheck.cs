﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Microsoft.Xna.Framework;
using SuperCrateHoarders.Scenes;
using Nez.Tiled;
using SuperCrateHoarders.Constants;
using Nez.Sprites;

namespace SuperCrateHoarders.Components
{
    public class BulletCheck : Component, ITriggerListener
    {
        public int hp = 2;
        Sprite sprite;

        bool imDead = false;

        public override void onAddedToEntity()
        {
            base.onAddedToEntity();
            sprite = entity.getComponent<Sprite>();
        }
        public void onTriggerEnter(Collider other, Collider local)
        {
            if(other.entity.name == Strings.BULLET || other.entity.name == Strings.EXPLOSIVE)
            {
                if(other.entity.name == Strings.BULLET)
                {
                    other.entity.destroy();
                }
                else if(other.entity.name == Strings.EXPLOSIVE)
                {
                    hp -= 1;
                }
                hp -= 1;
                sprite.color = Color.Red;
                if (hp <= 0 && !imDead)
                {
                    imDead = true;
                    var startFrame = local.entity.getComponent<PlayerAnimationController>().startFrame;
                    var dir = local.entity.position - other.entity.position;
                    Entities.BasicEnemy.CreateDeadEnemy(local.entity.scene, local.entity.position, startFrame, dir);
                    local.entity.destroy();
                    var scene = (Scenes.GameScene)entity.scene;
                    scene.PlaySoundEffect(Constants.Strings.ENEMY_HIT_SOUND);
                }
                else
                {
                    Core.schedule(0.1f, (timer) => sprite.color = Color.White);
                    var scene = (Scenes.GameScene)entity.scene;
                    scene.PlaySoundEffect(Constants.Strings.ENEMY_DEAD_SOUND);
                }
            }
            if (other.name == Strings.OFFSCREEN_COLLIDER)
            {

                // we respawning!
                //swap sprites & double speed
                var animController = local.entity.getComponent<PlayerAnimationController>();
                var sprite = local.entity.getComponent<Sprite<PlayerAnimationController.PlayerAnimations>>();
                var controller = local.entity.getComponent<BasicEnemyController>();
                Core.schedule(0.5f, (timer) =>
                {
                    var s = local.addComponent(new Sprite<PlayerAnimationController.PlayerAnimations>());
                    s.renderLayer = (int)Constants.RenderLayers.Default;
                    s.followParentEntityRotation = false;
                    var newController = local.entity.addComponent(new PlayerAnimationController(s, 64));
                    controller.moveSpeed *= 1.5f;
                    controller.ChangeAnimationContoller(newController);
                    local.entity.removeComponent(animController);
                    local.entity.removeComponent(sprite);
                    local.entity.position = EnemySpawner.spawnLocation;
                    var scene = (Scenes.GameScene)entity.scene;
                    scene.PlaySoundEffect(Constants.Strings.ENEMY_POWERUP_SOUND);
                });
            }
        }

        public void onTriggerExit(Collider other, Collider local)
        {
        }
    }
}
