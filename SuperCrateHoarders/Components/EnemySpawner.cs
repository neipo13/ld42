﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Microsoft.Xna.Framework;
using Nez.Tiled;
using SuperCrateHoarders.Entities;

namespace SuperCrateHoarders.Components
{
    public class EnemySpawner : SceneComponent
    {
        TiledTileLayer collisionLayer;
        public static Vector2 spawnLocation = new Vector2(Engine.designWidth / 2, -16);

        float maxTimeBetweenSpawns = 3.5f;
        float minTimeBetweenSpawns = 3.0f;
        float adjustmentAmt = 0.9f;

        bool spawning = true;

        float spawnTimer;

        int counter = 0;
        int spawnsUntilUpgrade = 5;
        

        public EnemySpawner()
        {
            Engine.gameEventEmitter.addObserver(GameEvents.GameOver, () => spawning = false);
        }
        
        public void SetCollisionLayer(TiledTileLayer collisionLayer)
        {
            this.collisionLayer = collisionLayer;            
        }

        public void AdjustSpawnTimers()
        {
            maxTimeBetweenSpawns *= adjustmentAmt;
            minTimeBetweenSpawns *= adjustmentAmt;
        }

        public void Spawn()
        {
            BasicEnemy.Create(this.scene, spawnLocation, collisionLayer);
            spawnTimer = Nez.Random.range(minTimeBetweenSpawns, maxTimeBetweenSpawns);
            var scene = (Scenes.GameScene)this.scene;
            scene.PlaySoundEffect(Constants.Strings.ENEMY_SPAWN_SOUND);
            counter++;
            if(counter > spawnsUntilUpgrade)
            {
                spawnsUntilUpgrade++;
                counter = 0;
                AdjustSpawnTimers();
            }
        }

        public override void update()
        {
            base.update();
            if (!spawning) return;
            if(spawnTimer > 0f)
            {
                spawnTimer -= Time.deltaTime;
            }
            else
            {
                Spawn();
            }
        }


    }
}
