﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez;
using Microsoft.Xna.Framework;
using SuperCrateHoarders.Scenes;
using Nez.Tiled;
using SuperCrateHoarders.Constants;
using Nez.Sprites;
using SuperCrateHoarders.Entities;

namespace SuperCrateHoarders.Components
{
    public class CrateCheck : Component, ITriggerListener
    {
        TiledTileLayer tileLayer;

        public CrateCheck(TiledTileLayer layer)
        {
            tileLayer = layer;
        }
        public void onTriggerEnter(Collider other, Collider local)
        {
            if(other.name == Strings.CRATE)
            {
                //spawn new
                var scene = (GameScene)other.entity.scene;
                scene.score += 1;
                scene.SpawnCrate(tileLayer);
                var sprites = other.entity.getComponents<Sprite>();
                foreach(Sprite s in sprites)
                {
                    if(s.name == Strings.USED_CRATE)
                    {
                        s.enabled = true;
                    }
                    else if(s.name == Strings.NEW_CRATE)
                    {
                        s.enabled = false;
                    }
                }
                //change gun
                var controller = local.entity.getComponent<PlayerController>();
                controller.ChangeWeapon(Constants.Weapons.GetWeapon(local.entity, controller.weapon.weaponName));
                scene.DisplayWeaponName(local.entity.position - new Vector2(0, 16), controller.weapon.weaponName);
                scene.PlaySoundEffect(Constants.Strings.CRATE_PICKUP_SOUND);
            }
            if(other.name == Strings.OFFSCREEN_COLLIDER || other.name == Strings.HITBOX)
            {
                // we dead
                Player.CreateDeadPlayer(other.entity.scene, local.entity.position);
                Engine.gameEventEmitter.emit(GameEvents.GameOver);
                var scene = (Scenes.GameScene)this.entity.scene;
                scene.PlaySoundEffect(Constants.Strings.PLAYER_DEAD_SOUND);
            }
        }

        public void onTriggerExit(Collider other, Collider local)
        {
            if (other.name == Strings.CRATE)
            {
                //become a tile somehow
                var tile = new TiledTile(19);
                var adjustedPostion = other.entity.position + TiledHelpers.tiledPositionAdjustment;
                var tileX = adjustedPostion.X / 16;
                var tileY = adjustedPostion.Y / 16;
                tile.x = (int)tileX;
                tile.y = (int)tileY;
                tileLayer.setTile(tile);

                //get rid of self
                other.entity.removeAllComponents();
                var col = other.entity.addComponent(new BoxCollider(16, 16));
                col.physicsLayer = PhysicsLayers.tiles;
            }
        }
    }
}
