﻿using Nez;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nez.Tiled;
using Nez.Sprites;
using Nez.Audio;
using SuperCrateHoarders.Util;
using Microsoft.Xna.Framework.Audio;
using SuperCrateHoarders.Interfaces;
using SuperCrateHoarders.Components.Weapons;

namespace SuperCrateHoarders.Components
{
    public class PlayerController : Component, IUpdatable
    {
        TiledMapMover mover;
        TiledMapMover.CollisionState collisionState = new TiledMapMover.CollisionState();
        BoxCollider moveBox;
        Sprite<PlayerAnimationController.PlayerAnimations> sprite;
        PlayerAnimationController animationController;
        BoxCollider hitBox;

        Direction direction = Direction.Right;
        ColliderTriggerHelper triggerHelper;

        AudioSource swingAudioSource;


        float moveSpeed = 175f;
        Vector2 velocity;
        float gravity = 800f;
        float runAccel = 1000f;
        float friction = 400f;
        Vector2 maxSpeedVec;
        bool isGrounded => collisionState.below;
        bool isMovingHorizontal => velocity.X > 0f || velocity.X < 0f;
        float jumpHeight = 16 * 5 + 4; //16px tiles * tiles high + buffer
        bool canJumpThisFrame
        {
            get { return collisionState.below || (offGroundInputBufferTimer > 0 && justJumpedBufferTimer <= 0); }
        }
        int offGroundInputBufferFrames = 8;
        int offGroundInputBufferTimer = 0;
        int landingInputBufferFrames = 4;
        int landingInputBufferTimer = 0;
        int justJumpedBufferTimer = 0;

        public IWeapon weapon;

        InputHandler input;
        public override void onAddedToEntity()
        {
            input = entity.getComponent<InputHandler>();
            mover = entity.getComponent<TiledMapMover>();
            animationController = entity.getComponent<PlayerAnimationController>();
            sprite = animationController.sprite;
            var boxes = entity.getComponents<BoxCollider>();
            moveBox = boxes.FirstOrDefault(c => c.name == Constants.Strings.MOVEBOX);
            hitBox = boxes.FirstOrDefault(c => c.name == Constants.Strings.HITBOX);
            velocity = Vector2.Zero;
            maxSpeedVec = new Vector2(moveSpeed * 2f, moveSpeed * 3f);
            triggerHelper = new ColliderTriggerHelper(entity);
            Engine.gameEventEmitter.addObserver(GameEvents.GameOver, OnGameOver);

            //swingAudioSource = new AudioSource();
            //var effect = entity.scene.content.Load<SoundEffect>("audio/slash_sound");
            //swingAudioSource.addSoundEffect(effect);
            //swingAudioSource.setPitchRange(0.8f, 1.2f);
            
        }

        public void OnGameOver()
        {
            entity?.destroy();
        }

        public void update()
        {
            if (Time.timeScale < 0.1f) return;
            move();
            triggerHelper.update();
            if (velocity.X > 0)
            {
                sprite.flipX = false;
                weapon.setFlipX(false);
            }
            else if (velocity.X < 0)
            {
                sprite.flipX = true;
                weapon.setFlipX(true);
            }

            if (!isGrounded || !isMovingHorizontal)
            {
                animationController.Play(PlayerAnimationController.PlayerAnimations.Idle);
            }
            else if (isGrounded && isMovingHorizontal)
            {
                animationController.Play(PlayerAnimationController.PlayerAnimations.Run);
            }

            if (input.Button2Input.isPressed)
            {
                Direction lr = Direction.Right;
                if (sprite.flipX)
                {
                    lr = Direction.Left;
                }
                weapon.Fire(lr);
            }

        }

        public void ChangeWeapon(IWeapon weapon)
        {
            var old = this.weapon;
            this.weapon = weapon;
            old?.RemoveSprites();
            entity.removeComponent((Component)old);
        }

        public void move()
        {
            //groundInputBuffer
            if (collisionState.wasGroundedLastFrame && !collisionState.below)
            {
                offGroundInputBufferTimer = offGroundInputBufferFrames;
            }

            //handle left/right
            var highEnoughToTurn = Math.Abs(velocity.X) >= moveSpeed * 3f / 4f;
            var direction = 0;
            if (highEnoughToTurn)
            {
                direction = Math.Sign(velocity.X);
            }
            if (Math.Abs(velocity.X) > moveSpeed && Math.Sign(velocity.X) == input.XInput)
                velocity.X = Mathf.approach(velocity.X, moveSpeed * input.XInput, friction * Time.deltaTime);  //Reduce back from beyond the max speed
            else
                velocity.X = Mathf.approach(velocity.X, moveSpeed * input.XInput, runAccel * Time.deltaTime);
            //drop through one way
            if (collisionState.isGroundedOnOneWayPlatform && input.YInput > 0 && input.Button1Input.isPressed)
            {
                entity.setPosition(new Vector2(entity.position.X, entity.position.Y + 1));
                collisionState.clearLastGroundTile();
                landingInputBufferTimer = 0;
            }
            //jump
            else if (input.Button1Input.isPressed)
            {
                if (canJumpThisFrame) Jump();
                else landingInputBufferTimer = landingInputBufferFrames;
            }
            // jump if you recently hit jump before you landed
            else if (landingInputBufferTimer > 0)
            {
                landingInputBufferTimer--;
                if (collisionState.below) Jump();
            }
            // handle variable jump height
            if (!collisionState.below && input.Button1Input.isReleased)
            {
                velocity.Y *= 0.5f;
            }


            //apply gravity
            velocity.Y += gravity * Time.deltaTime;

            velocity = Vector2.Clamp(velocity, -maxSpeedVec, maxSpeedVec);

            var oldPos = entity.position;
            mover.move(velocity * Time.deltaTime, moveBox, collisionState);

            if (collisionState.below)
            {
                sprite.rotation = 0;
            }
            else
            {
                sprite.rotation += (float)Math.PI / 8 * (sprite.flipX ? -1 : 1);
            }

            var leftSide = entity.position.X - moveBox.width / 2;
            var rightSide = entity.position.X + moveBox.width / 2;

            // dont let gravity just build while you're grounded
            if (collisionState.above || collisionState.below)
                velocity.Y = 0;
            // tick jump input buffer timer
            if (offGroundInputBufferTimer > 0)
                offGroundInputBufferTimer--;
            if (justJumpedBufferTimer > 0)
                justJumpedBufferTimer--;
        }

        void Jump()
        {
            velocity.Y = -Mathf.sqrt(2 * jumpHeight * gravity);
            landingInputBufferTimer = 0;
            justJumpedBufferTimer = offGroundInputBufferFrames;
            var scene = (Scenes.GameScene)entity.scene;
            scene.PlaySoundEffect(Constants.Strings.JUMP_SOUND);
        }        

        private void setDirection()
        {
            var absInputValues = new Vector2(Math.Abs(input.axialInput.X), Math.Abs(input.axialInput.Y));

            if (absInputValues.X > absInputValues.Y)
            {
                if (input.axialInput.X > 0f)
                {
                    direction = Direction.Right;
                }
                else if (input.axialInput.X < 0f)
                {
                    direction = Direction.Left;
                }
            }
            else
            {
                if (input.axialInput.Y > 0f)
                {
                    direction = Direction.Down;
                }
                else if (input.axialInput.Y < 0f)
                {
                    direction = Direction.Up;
                }
            }
        }
    }

}
