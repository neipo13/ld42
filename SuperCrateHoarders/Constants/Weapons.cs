﻿using Nez;
using SuperCrateHoarders.Components.Weapons;
using SuperCrateHoarders.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperCrateHoarders.Constants
{
    public class Weapons
    {
        public const int Pistol = 0;
        public const int DualPistol = 1;
        public const int Shotgun = 2;
        public const int AlienBlaster = 3;
        public const int RocketLauncher = 4;
        public const int GrenadeLauncher = 5;
        public const int CrateGun = 6;


        public static IWeapon GetWeapon(Entity e, string currentWeapon)
        {
            var rand = Nez.Random.nextInt(7);
            switch (rand)
            {
                case Pistol:
                    if (currentWeapon == Strings.PISTOL) return GetWeapon(e, currentWeapon);
                    return e.addComponent<Pistol>();
                case DualPistol:
                    if (currentWeapon == Strings.DUAL_PISTOLS) return GetWeapon(e, currentWeapon);
                    return e.addComponent<DualPistols>();
                case Shotgun:
                    if (currentWeapon == Strings.SHOTGUN) return GetWeapon(e, currentWeapon);
                    return e.addComponent<Shotgun>();
                case AlienBlaster:
                    if (currentWeapon == Strings.ALIEN_BLASTER) return GetWeapon(e, currentWeapon);
                    return e.addComponent<AlienBlaster>();
                case RocketLauncher:
                    if (currentWeapon == Strings.ROCKET_LAUNCHER) return GetWeapon(e, currentWeapon);
                    return e.addComponent<RocketLauncher>();
                case GrenadeLauncher:
                    if (currentWeapon == Strings.GRENADE_LAUNCHER) return GetWeapon(e, currentWeapon);
                    return e.addComponent<GrendadeLauncher>();
                case CrateGun:
                    if (currentWeapon == Strings.CRATE_GUN) return GetWeapon(e, currentWeapon);
                    return e.addComponent<CrateGun>();
            }
            //default to the pistol if all else fails
            return e.addComponent<Pistol>(); ;
        }
    }
}
