﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperCrateHoarders.Constants
{
    public enum RenderLayers
    {
        Default = 0,
        Background = 2,
        Weapons = -1,
        Explosions = 1
    }
}
