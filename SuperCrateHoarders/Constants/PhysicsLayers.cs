﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperCrateHoarders.Constants
{
    public static class PhysicsLayers
    {
        public static int tiles = 1 << 0;
        public static int move = 1 << 1;
        public static int player_hit = 1 << 2;
        public static int player_hurt = 1 << 3;
        public static int crate = 1 << 4;
        public static int bullet = 1 << 5;
        public static int enemy_move = 1 << 6;
        public static int offscreen_collider = 1 << 7;
        public static int enemy_hit = 1 << 8;
        public static int enemy_hurt = 1 << 9;
        public static int explosion = 1 << 10;
    }
}
