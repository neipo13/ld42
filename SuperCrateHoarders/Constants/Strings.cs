﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperCrateHoarders.Constants
{
    public class Strings
    {
        public const string HITBOX = "hitbox";
        public const string HURTBOX = "hurtbox";
        public const string MOVEBOX = "movebox";
        public const string CRATE = "crate";
        public const string NEW_CRATE = "newcrate";
        public const string USED_CRATE = "usedcrate";
        public const string OFFSCREEN_COLLIDER = "offscreencollider";
        public const string BULLET = "bullet";
        public const string EXPLOSIVE = "explosive";
        public const string PLAYER = "player";


        public const string PISTOL = "pistol";
        public const string DUAL_PISTOLS = "dual pistols";
        public const string SHOTGUN = "shotgun";
        public const string ALIEN_BLASTER = "alien blaster";
        public const string ROCKET_LAUNCHER = "rocket launcher";
        public const string GRENADE_LAUNCHER = "grenade launcher";
        public const string CRATE_GUN = "crate launcher";

        public const string HIGHSCORE_FILE_NAME = "highscore.dat";

        public const string ENEMY_DEAD_SOUND = "audio/ld42-enemy-dead";
        public const string ENEMY_HIT_SOUND = "audio/ld42-enemy-hit";
        public const string ENEMY_SPAWN_SOUND = "audio/ld42-enemy-powerup";
        public const string JUMP_SOUND = "audio/ld42-jump";
        public const string PISTOL_SOUND = "audio/ld42-pistol-shoot";
        public const string PLAYER_DEAD_SOUND = "audio/ld42-player-dead";
        public const string ENEMY_POWERUP_SOUND = "audio/ld42-powerup2";
        public const string SHOTGUN_SOUND = "audio/ld42-shotgun";
        public const string CRATE_PICKUP_SOUND = "audio/ld42-crate-pickup";
        public const string BAZOOKA_SOUND = "audio/ld42-bazooka";
        public const string GRENADE_LAUNCHER_SOUND = "audio/ld42-grenade-launch";
        public const string LAZER_SOUND = "audio/ld42-lazer";
        public const string EXPLOSION_SOUND = "audio/ld42-explosion";
    }
}
