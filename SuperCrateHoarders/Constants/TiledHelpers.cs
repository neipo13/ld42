﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperCrateHoarders.Constants
{
    public static class TiledHelpers
    {
        public static Vector2 tiledPositionAdjustment = new Vector2(0, -8);
        public static Vector2 tiledPositionAdjustment2 = new Vector2(8, 8);

        public static Vector2 AdjustTiledPosition(Vector2 position) => position + tiledPositionAdjustment;
    }
}
